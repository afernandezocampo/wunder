<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentDataId;

    /**
     * @ORM\ManyToOne(targetEntity="BankAccount", inversedBy="payments")
     */
    private $bankAccount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(string $paymentDataId): self
    {
        $this->paymentDataId = $paymentDataId;

        return $this;
    }

    /**
     * @return BankAccount
     */
    public function getBankAccount(): BankAccount
    {
        return $this->bankAccount;
    }

    /**
     * @param mixed $bankAccount
     * @return Payment
     */
    public function setBankAccount($bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

}
