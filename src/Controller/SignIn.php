<?php

namespace App\Controller;

use App\Entity\BankAccount;
use App\Entity\Payment;
use App\Entity\User;
use App\Service\PaymentAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class SignIn extends Controller
{
    private $paymentapi;

    public function __construct(PaymentAPI $payment)
    {
        $this->paymentapi = $payment;
    }

    public function StepOne(Request $request)
    {
        $session = $request->getSession();

        $resultChecking = $this->statusStep($request);
        if(!empty($resultChecking)) return $resultChecking;


        if($request->getMethod() == Request::METHOD_POST){
            $firsname = $request->request->get("firstname");
            $lastname = $request->request->get("lastname");
            $telephone = $request->request->get("telephone");

            //Validation if necessary
            $user = empty($session->get("user")) ? new User() : $session->get("user");
            $user->setFirstname($firsname);
            $user->setLastname($lastname);
            $user->setTelephone($telephone);


            $session->set("user",$user);
            $session->set("step","steptwo");

            return $this->redirectToRoute("steptwo");
        }

        $session = $request->getSession();
        $user = !empty($session->get("user")) ? $session->get("user") : new User();

        return $this->render("signin/stepone.html.twig",array(
            'user' => $user
        ));
    }

    public function stepTwo(Request $request)
    {
        $resultChecking = $this->statusStep($request);
        if(!empty($resultChecking)) return $resultChecking;

        $session = $request->getSession();
        /** @var User $user */
        $user = $session->get("user");

        if($request->getMethod() == Request::METHOD_POST) {
            $address = $request->request->get("address");
            $housenumber = $request->request->get("housenumber");
            $zipcode = $request->request->get("zipcode");
            $city = $request->request->get("city");

            $user->setAddress($address);
            $user->setHousenumber($housenumber);
            $user->setZipcode($zipcode);
            $user->setCity($city);

            $session = $request->getSession();
            $session->set("user",$user);
            $session->set("step","stepthree");

            return $this->redirectToRoute("stepthree");
        }

        return $this->render("signin/steptwo.html.twig",array(
            'user' => $user
        ));
    }

    public function stepThree(Request $request)
    {
        $error = $request->query->get("error");

        $resultChecking = $this->statusStep($request);
        if(!empty($resultChecking)) return $resultChecking;

        $session = $request->getSession();

        if($request->getMethod() == Request::METHOD_POST){
            $owner = $request->request->get("owner");
            $iban = $request->request->get("iban");

            $session = $request->getSession();
            /** @var User $user */
            $user = $session->get("user");

            $bankAccount = new BankAccount();
            $bankAccount->setAccountOwner($owner);
            $bankAccount->setIban($iban);
            $bankAccount->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($bankAccount);

            $em->flush();

            $response = $this->paymentapi->PaymentSubmission($user,$bankAccount);

            if($response['code'] === 200){
                $payment = new Payment();
                $payment->setPaymentDataId($response['paymentDataId']);

                $bankAccount->addPayment($payment);
                $em->persist($bankAccount);
                $em->persist($payment);
                $em->flush();


                $session->remove("step");
            }else{
                return $this->redirectToRoute("stepthree",array("error"=>"1"));
            }

            return $this->redirectToRoute("summary",array("paymentDataId" => $response['paymentDataId']));
        }
        return $this->render("signin/stepthree.html.twig",array("error" => $error));
    }

    public function summary(Request $request){
        $session = $request->getSession();
        $user = $session->get("user");
        $session->remove("user");
        $paymentDataId = $request->query->get("paymentDataId");

        return $this->render('signin/summary.html.twig',array(
            'user'=>$user,
            'paymentDataId' => $paymentDataId,
        ));
    }

    public function previousStep(Request $request)
    {
        $step = $request->request->get("previousstep");
        $session = $request->getSession();
        $step = $session->set("step",$step);
        $resultChecking = $this->statusStep($request);
        if(!empty($resultChecking)) return $resultChecking;
    }

    public function statusStep(Request $request)
    {
        $session = $request->getSession();
        $step = $session->get("step");
        if(empty($step)) {
            $session->set("step","stepone");
            $step = "stepone";
        }
        $route = $request->get("_route");

        if($route === $step){
            return null;
        }

        switch ($step){
            case "stepone":
            case "steptwo":
            case "stepthree":
                return $this->redirectToRoute($step);
                break;
        }
        return null;
    }
}