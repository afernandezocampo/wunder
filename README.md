Introduction: I used Symfony a MVC framework.
What I could have done better is the structure of the project where the business logic could have been encapsulated in classes / services to make it more testable.

Also I used sessions to established where the application is at. I use a code that is repeated in certain route paths. To avoid repeating logic on every page I could have attached a service to an system event.

Also it is certain that a better architecture/pattern such as MVP could be a better solution but due to lack of time I used symfony. MVP would allow the user to interact with the view and update the model behind and gives a feedback. That would be useful for the button go back where I was forced to use a post / form (could have been ajax) in order to alter the session, that stores the step progress.

No tests were provided, not css work or any framework. I chose to stay minimal. Also very important: the service API was not working and therefore I had to use a mock response.

#Installation
- Once the project pulled from bitbucket.
``cd wunder`` 

- Install the dependencies
``composer install``

- Update the .env file with the proper settings. I pushed it on purpose eventhough in projects we should not be pushing it into the git repo. Complete the user, password, host and port in the url regarding the database. line 16 ````DATABASE_URL=mysql://root:@127.0.0.1:3306/wunder````

- Now you can create the database with the command:

``php bin/console doctrine:database:create``

- Update the database schema:

``php bin/console doctrine:schema:update --force``

- Launch a new window console to launch the symfony server
``php bin/console server:run``

- And now you can navigate into the project to check the application at:

``http://localhost:8000``

